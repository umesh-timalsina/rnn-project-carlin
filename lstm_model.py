import tensorflow as tf
import numpy as np
from tensorflow.contrib import rnn
from utils import *
from tensorflow.contrib.layers import fully_connected

tokens = read_tokenize(['./you_are_all_diseased.txt',\
                             './jammin_in_newyork.txt'])
num_batches, train_X, train_y, op_class_dict, rev_dict = generate_training_batches(tokens, 50, './main_dict.pickle', 20)
print(train_y[0].shape)
## Hyper parameters ##
output_dim = len(op_class_dict)
input_dim = 300
seq_size = 20
hidden_dimension = 300
n_epochs = 1000


W_out = tf.Variable(tf.random_normal([hidden_dimension, 1]), name='W_out')
b_out = tf.Variable(tf.random_normal([1]), name='b_out')

cell = rnn.BasicLSTMCell(hidden_dimension)
X = tf.placeholder(tf.float32, [None, seq_size, input_dim])
y = tf.placeholder(tf.float32, [None, output_dim])
outputs, states = tf.nn.dynamic_rnn(cell, X, dtype=tf.float32)
stacked_rnn_outputs = tf.reshape(outputs, [-1, hidden_dimension])
stacked_outputs = fully_connected(stacked_rnn_outputs, output_dim,\
                    activation_fn=None)
outputs = tf.reshape(stacked_outputs, [-1, seq_size, output_dim])
logits = outputs[:, -1, :]   # Select the last output
error = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
train_op = optimizer.minimize(error)
init = tf.global_variables_initializer()
saver = tf.train.Saver()
# Train the saver
# with tf.Session() as sess:
#     init.run()
#     train_y_oh = tf.one_hot(train_y, depth=output_dim).eval()
#     for i in range(n_epochs):
#         for j in range(num_batches):
#              _, loss_val = sess.run([train_op, error], feed_dict={X: train_X[j] , y: train_y_oh[j]})
#         print('Epoch: %d , loss: %f'%(i, loss_val))
#         saver.save(sess, './ckpts/model_op.ckpt')

## Generate Text
words = ['hello', 'fuckers', '.', 'it', 'has', 'been', 'a', 'little', 'while',\
            'since', 'I', 'met', 'you', 'guys', '.', 'its', 'been',\
            'a', 'long', 'ride']
generated_sentence = words.copy()
seed_vector = np.expand_dims(generate_seed_vectors(words, './main_dict.pickle'), axis=0)
# print(seed_vector.shape)
with tf.Session() as sess:
    saver.restore(sess, './ckpts/model_op.ckpt')
    print('Model Restored....')
    for i in range(200):
        op_word_vector = sess.run([logits], feed_dict={X: seed_vector})
        op_word_vector_smaxed = sess.run(tf.nn.softmax(op_word_vector[0][0]))
        op_word_index = sess.run(tf.argmax(op_word_vector_smaxed))
        target_word = rev_dict[op_word_index]
        words.append(target_word)
        generated_sentence.append(target_word)
        print(' '.join(words))
        words = words[1:]
        seed_vector = np.expand_dims(generate_seed_vectors(words, './main_dict.pickle'), axis=0)
        # print(len(op_word_vector_smaxed))
    print('Generated Sentences: ')
    print(' '.join(generated_sentence))