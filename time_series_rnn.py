import tensorflow as tf
from tf.contrib.layers import fully_connected

n_steps = 20
n_inputs = 1
n_neurons = 100
n_outputs = 1

X = tf.placeholder(tf.float32, [None, n_steps, n_inputs])
y = tf.placeholder(tf.float32, [None, n_steps, n_outputs])
cell =tf.contrib.rnn.BasicLSTMCell(num_units=n_neurons)
rnn_outputs, states	= tf.nn.dynamic_rnn(cell, X, dtype=tf.float32)
stacked_rnn_outputs = tf.reshape(rnn_outputs, [-1, n_neurons])
outputs = fully_connected(stacked_rnn_outputs, n_outputs, activation_fn=None)
outputs	=	tf.reshape(stacked_outputs,	[-1,	n_steps,	n_outputs])
learning_rate = 0.001
loss = tf.reduce_mean(tf.square(outputs-y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
training_op	=	optimizer.minimize(loss)
init = tf.global_variables_initializer()

n_epochs = 100000